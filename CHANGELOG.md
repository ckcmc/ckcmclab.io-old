# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres (loosely[^looseley_semver]) to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

[^looseley_semver]: We will likely only need MAJOR and MINOR version for this project.


## [Unreleased]
### Added
- Initial version


## x.y.z - yyyy-mm[-dd]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security
